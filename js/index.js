$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal se está mostrando');

        $('#contactoBtn1').removeClass('btn-outline-success');
        $('#contactoBtn1').addClass('btn-primary');
        $('#contactoBtn1').prop('disabled', true);

        $('#contactoBtn2').removeClass('btn-outline-success');
        $('#contactoBtn2').addClass('btn-primary');
        $('#contactoBtn2').prop('disabled', true);

        $('#contactoBtn3').removeClass('btn-outline-success');
        $('#contactoBtn3').addClass('btn-primary');
        $('#contactoBtn3').prop('disabled', true);

        $('#contactoBtn4').removeClass('btn-outline-success');
        $('#contactoBtn4').addClass('btn-primary');
        $('#contactoBtn4').prop('disabled', true);

        $('#contactoBtn5').removeClass('btn-outline-success');
        $('#contactoBtn5').addClass('btn-primary');
        $('#contactoBtn5').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('El modal se está ocultando');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal se oculto');

        $('#contactoBtn1').removeClass('btn-primary');
        $('#contactoBtn1').addClass('btn-outline-success');
        $('#contactoBtn1').prop('disabled', false);

        $('#contactoBtn2').removeClass('btn-primary');
        $('#contactoBtn2').addClass('btn-outline-success');
        $('#contactoBtn2').prop('disabled', false);

        $('#contactoBtn3').removeClass('btn-primary');
        $('#contactoBtn3').addClass('btn-outline-success');
        $('#contactoBtn3').prop('disabled', false);

        $('#contactoBtn4').removeClass('btn-primary');
        $('#contactoBtn4').addClass('btn-outline-success');
        $('#contactoBtn4').prop('disabled', false);

        $('#contactoBtn5').removeClass('btn-primary');
        $('#contactoBtn5').addClass('btn-outline-success');
        $('#contactoBtn5ñ').prop('disabled', false);
    });
});